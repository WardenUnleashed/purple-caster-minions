 <div <?php post_class(); ?>>
          <a href="<?php the_permalink(); ?> "><?php if( get_post_type() == 'event'){ ?>
				<?php $image = get_field('feature_img'); ?>
                <div class="event-date<?php if ($image != null) { echo "-feature"; } ?>">
               	<span class="month"><?php the_field('tournament_month'); ?></span>
                <?php the_field('tournament_day'); ?>
                      
                </div>
				<div class="event-title<?php if ($image != null) { echo "-feature"; } ?>">
				
                <?php if ($image != null) {?>
                        <img src="<?php  echo $image['url'];?>" alt="<?php echo $image['alt']; ?>" class="feature-event-image"/>
                <?php } else { ?>
                   
                    	<h2>
                        <?php the_title(); ?>
                    </h2>
             <?php } ?>
             	</div>
          <?php }else { ?>
				<?php $image = get_field('feature_img'); 
                if ($image != null) {?>
                   
                        <img src="<?php  echo $image['url'];?>" alt="<?php echo $image['alt']; ?>" class="feature"/>
                <?php } else { ?>
                   
                    	<h2 class="title">
                        <?php the_title(); ?>
                    </h2>
             <?php } ?>
      <?php } ?>
      <?php if( get_post_type() == 'event' && $image == null){ ?>
      	<div class="clear"></div>
        <?php } ?></a>
        <div class="excerpt">
        <?php the_excerpt(); ?>
        </div>
            </div>