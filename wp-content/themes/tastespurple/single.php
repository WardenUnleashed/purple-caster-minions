<?php add_action('wp_head', 'hook_css');
function hook_css(){
	$output = '<link rel="stylesheet" href="' . get_template_directory_uri() . '/css/single.css" type="text/css">';
	echo $output;
}
?>
<?php get_header(); ?>
<main>
    <div class="container">
        <article>
         
        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
         
        <div <?php post_class() ?> id="post-<?php the_ID(); ?>">
        <div class="info">
        	<h2><?php the_title(); ?></h2>
            <?php $image = get_field('feature_img'); ?>
            <?php if ($image != null) {?>
                  <img src="<?php  echo $image['url'];?>" alt="<?php echo $image['alt']; ?>" class="feature-image"/>
         	<?php } ?>
        <div>
        <?php 
		the_content('<p>Read the rest of this entry &raquo;</p>'); 

		?>
         
        <?php wp_link_pages(array('before' => '<p><strong>Pages:</strong> ', 'after' => '</p>', 'next_or_number' => 'number')); ?>
              
        </div>
        </div>
         
        <?php comments_template(); ?>
         
        <?php endwhile; else: ?>
         
        <p>Sorry, no posts matched your criteria.</p>
         
        <?php endif; ?>
         
        </div>
         </article>
             <?php 	if ( comments_open() || get_comments_number() ) {
					?><div class="comments">	
						<?php comments_template(); ?>
					</div>
             <?php } ?>
    </div>
</main>
<?php get_footer(); ?>
