// JavaScript Document

jQuery(document).ready(function($) {
    $(".menu").hide();
    $(".toggle").click(function() {
        $(".menu").slideToggle(500);
    });
	var $container = $('#infinite');
	// initialize
	$container.masonry({
	  columnWidth: '.grid-size',
	  itemSelector: '.hentry'
	});
	infinite_count = 0;
     $( document.body ).on( 'post-load', function () {
		infinite_count = infinite_count + 1;
		var $selector = $('#infinite-view-' + infinite_count);
		var $elements = $selector.find('.hentry');
		$container.masonry( 'appended', $elements, 'isAnimatedFromBottom' );
     });

});

     
     // Triggers re-layout on infinite scroll
