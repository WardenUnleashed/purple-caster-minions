<?php
	defined('ABSPATH') or die("No script kiddies please!");
?>

<!DOCTYPE html>
<html>
    <head>
            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <title><?php wp_title(); ?></title>
            <meta name="description" content="<?php bloginfo('description'); ?>">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/normalize.css" type='text/css'>
			<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,800,700|Oswald' rel='stylesheet' type='text/css'>
			<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.min.css" type='text/css'>
            <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/style.css" type='text/css'>
            
            <?php wp_head(); ?> 
    </head>
	<body>
        <nav class="mobile">
            <div id="toggle-bar">
                <a class="top" href="<?php echo site_url(); ?>">PCM</a>
                <a class="navicon toggle" href="#">MAIN MENU</a>
            </div>
			<?php
            
            $mobile = array(
                'theme_location'  => 'mobile-nav',
                'menu'            => '',
                'container'       => 'div',
                'container_class' => 'container',
                'container_id'    => '',
                'menu_class'      => 'menu',
                'menu_id'         => '',
                'echo'            => true,
                'fallback_cb'     => 'wp_page_menu',
                'before'          => '',
                'after'           => '',
                'link_before'     => '',
                'link_after'      => '',
                'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                'depth'           => 0,
                'walker'          => ''
            );
            
            wp_nav_menu( $mobile );
            
            ?>
        </nav>
        <nav class="full">
			<?php
            
            $defaults= array(
                'theme_location'  => 'full-nav',
                'menu'            => '',
                'container'       => 'div',
                'container_class' => 'container',
                'container_id'    => '',
                'menu_class'      => 'menu-full',
                'menu_id'         => '',
                'echo'            => true,
                'fallback_cb'     => 'wp_page_menu',
                'before'          => '',
                'after'           => '',
                'link_before'     => '',
                'link_after'      => '',
                'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                'depth'           => 0,
                'walker'          => ''
            );
            
            wp_nav_menu( $defaults );
            
            ?>
         </nav>
        