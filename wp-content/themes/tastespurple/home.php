<?php get_header(); ?>
<main>
	<div class="container" id="infinite">
	<div class="grid-size"></div>
    <?php //Take all the posts, if they have a freature image attached display that otherwise it will display the title ?>
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <?php get_template_part( 'content', '' ); ?>
     <?php endwhile; else : ?>
        <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
     <?php endif; ?>
     </div>
</main>
<?php get_footer(); ?>
