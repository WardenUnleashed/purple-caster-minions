<?php get_header(); ?>
<main>
	<div class="container" id="infinite">
	<div class="grid-size"></div>
   <?php $args = array(
	'type'            => 'postbypost',
	'limit'           => '',
	'format'          => 'html', 
	'before'          => '',
	'after'           => '',
	'show_post_count' => false,
	'echo'            => 1,
	'order'           => 'DESC'
); ?>
<?php wp_get_archives( $args ); ?> 
     </div>
</main>
<?php get_footer(); ?>
