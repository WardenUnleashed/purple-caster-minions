<?php
function register_my_menus() {
  register_nav_menus(
    array(
      'mobile-nav' => __( 'Mobile Navigation' ),
      'full-nav' => __( 'Full Screen Navigation' )
    )
  );
}
add_action( 'init', 'register_my_menus' );

// Show posts of 'post', and 'event' post types on home page
add_action( 'pre_get_posts', 'add_my_post_types_to_query' );

function add_my_post_types_to_query( $query ) {
  if ( is_home() && $query->is_main_query() )
    $query->set( 'post_type', array( 'post', 'event' ) );
  return $query;
}

// Load jquery
if (!is_admin()) add_action("wp_enqueue_scripts", "my_jquery_enqueue", 11);
function my_jquery_enqueue() {
    wp_deregister_script('jquery');
    wp_register_script('jquery', "http" . ($_SERVER['SERVER_PORT'] == 443 ? "s" : "") . "://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js", false, null, true);
    wp_enqueue_script('jquery');
}

function excerpt_read_more_link($output) {
 global $post;
 return $output . '<a href="'. get_permalink($post->ID) . '"> Read More...</a>';
}
add_filter('the_excerpt', 'excerpt_read_more_link');

add_theme_support( 'infinite-scroll', array(
    'container' => 'infinite',
    'footer' => false,
	'wrapper' => true,
) );
add_action( 'init', 'add_theme_support' );
?>