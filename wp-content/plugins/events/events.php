<?php
   /*
   Plugin Name: league-events
   Plugin URI: N/A
   Description: a plugin to allow events to be created and managed within Wordpress
   Version: 0.1
   Author: Andrew Johnson
   Author URI: http://students.washington.edu/andr3wj
   License: GPL2
   */
   
   // Make sure we don't expose any info if called directly
if ( !function_exists( 'add_action' ) ) {
	echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
	exit;
}

function register_lol_events() {
 
    $labels = array(
        'name' => _x( 'Events', 'events' ),
        'singular_name' => _x( 'Event', 'event' ),
        'add_new' => _x( 'Add New', 'event' ),
        'add_new_item' => _x( 'Add New Event', 'event' ),
        'edit_item' => _x( 'Edit Event', 'event' ),
        'new_item' => _x( 'New Event', 'event' ),
        'view_item' => _x( 'View Event', 'event' ),
        'search_items' => _x( 'Search Events', 'event' ),
        'not_found' => _x( 'No events found', 'event' ),
        'not_found_in_trash' => _x( 'No events found in trash', 'event' ),
        'parent_item_colon' => _x( 'Parent Event:', 'event' ),
        'menu_name' => _x( 'Events', 'event' ),
    );
 
    $args = array(
        'labels' => $labels,
        'hierarchical' => true,
        'description' => 'Events filterable by category',
        'supports' => array( 'title', 'editor', 'author', 'comments', 'revisions', 'page-attributes' ),
        'taxonomies' => array( 'categories' ),
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 5,
        'menu_icon' => 'dashicons-megaphone',
        'show_in_nav_menus' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'has_archive' => true,
        'query_var' => true,
        'can_export' => true,
        'rewrite' => true,
        'capability_type' => 'post'
    );
 
    register_post_type( 'event', $args );
}
 
add_action( 'init', 'register_lol_events' );
 
function events_taxonomy() {
    register_taxonomy(
        'categories',
        'event',
        array(
            'hierarchical' => true,
            'label' => 'Categories',
            'query_var' => true,
            'rewrite' => true
        )
    );
}
add_action( 'init', 'events_taxonomy');
?>